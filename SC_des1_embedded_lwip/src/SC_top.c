/*
 * SC_top.c
 *
 *  Created on: Feb 22, 2021
 *      Author: DanWinkelstein
 */

#include "SC_des1.h"
#include "lwip/err.h"
#include "lwip/tcp.h"
#include "lwip/inet.h"


// #defines

// Global definitions


// global interrupt event registers and flags
VOLT_INT_EVENT_T voltage_interrupt_event;
FREQ_INT_EVENT_T frequency_interrupt_event;
KEYPAD_INTERRUPT_T keypad_interrupt;
CURRENT_INT_EVENT_T current_interrupt_event;
STREAM_INTERRUPT_T stream_interrupt;
uint32_t receive_data_fifo_occupancy;
uint32_t stream_interrupt_status_reg;
uint32_t fabric_interrupt_event;
CONTACTOR_DISENGAGE_T previousContactorStatus;
CIRCUIT_BREAKER_STATUS_T previousCircuitBreakerStatus;
VOLT_INTERRUPT_T VoltageStatus;
FREQ_INTERRUPT_T FreqStatus;
CURRENT_INTERRUPT_T CurrentStatus;
VOLT_INTERRUPT_T previousVoltageStatus;
FREQ_INTERRUPT_T previousFreqStatus;
CURRENT_INTERRUPT_T previousCurrentStatus;


volatile u32 realTimeDataA[EIGHT_SECONDS_DATA] __attribute__ ((aligned (64)));
volatile u32 realTimeDataB[EIGHT_SECONDS_DATA] __attribute__ ((aligned (64)));
volatile u32 realTimeDataC[EIGHT_SECONDS_DATA] __attribute__ ((aligned (64)));
int currentBuffer = 0;
int currentBufferPtr = 0;
int tcp_connection_status = 0;

extern volatile int TcpFastTmrFlag;
extern volatile int TcpSlowTmrFlag;
//extern static struct netif server_netif;
extern struct netif *echo_netif;

// this is the complete register map for the hardware
SC_DES1_T sc_des1_inst;   //out global register definition file
// create memory space for each of the individual register spaces
SNAPSHOT_MEMORY_T snapshot_data;
ALARM_REGISTER_T alarm_data;
SENSOR_FACTOR_T sensor_factor;
PCA9539_T pca9539;

ALARM_LIMIT_T alarm_limit_value; // this holds the alarm limits we plan to install

CONTACTOR_DISENGAGE_T contactor_setting;
extern XZDma ZDma;		/**<Instance of the ZDMA Device */
extern XScuGic xInterruptController;	/**< XIntc Instance */
extern XScuGic XZDmaIntc;



// function prototypes
extern int lwip_main_init();

extern "C" {
void tcp_fasttmr(void);
void tcp_slowtmr(void);
}
extern int transfer_data();
extern struct tcp_pcb *pcb;
err_t accept_callback(void *arg, struct tcp_pcb *newpcb, err_t err);

int copyRealTimeData();
void dummy_set_sensor_factor();
void dummy_set_alarm_limits(ALARM_LIMIT_T *alarm_limit_value);   // remove or rename when we get real limits

void voltage_event_clear_operation();
void voltage_event_set_operation();
void frequency_event_clear_operation();
void frequency_event_set_operation();
void current_event_clear_operation();
void current_event_set_operation();
void keypad_event_operation();
void stream_snapshot_event_operation();

// debug
void pio_voltage();
void xdma_test();
int sc_top();

#define NETWORKING
#ifdef NETWORKING
#define TIMER_IRPT_INTR		XPAR_XTTCPS_0_INTR
#define INTC_DIST_BASE_ADDR	XPAR_SCUGIC_0_DIST_BASEADDR
#endif

// main

#ifdef SNAPSHOT
int main() {
	sc_top();
}
#endif

#ifdef IPCTEST
int main(){
	char write_buffer[80];

	for(int i= 0; i< 10; i++){
		//01234567890123456789012345678901234567890123456789012345678901234567890123456789

		sprintf(write_buffer,"Line 1 count %d     Line 2              Hello world         IPC TEST            ",i);
		clear_LCD();
		LCD_output(write_buffer, 80);
		sleep(5);
	}

}
#endif

int sc_top() {
	int heartbeat = 0;
	bool firstAlarm = true;

	//    init_platform();


	sc_des1_inst.snapshot_data = &snapshot_data;
	sc_des1_inst.alarm_data = &alarm_data;
	sc_des1_inst.sensor_factor = &sensor_factor;
	sc_des1_inst.temperature = 0;
	sc_des1_inst.pca9539 = &pca9539;
	CONTROL_REG_T local_control_reg;
	local_control_reg.reg = 0;
	set_control_reg(&local_control_reg);
	CONTACTOR_DISENGAGE_T local_contactor_control;
	local_contactor_control.reg = 0;
	set_contactor(local_contactor_control);
	sc_des1_inst.device_id = ((uint64_t) Xil_In32(DNA_ADDRESS1) << 32) | (uint64_t) Xil_In32(DNA_ADDRESS0); // This is a unique 96-bit number assigned to the PS device,


#ifndef IPC_DISPLAY
	// initialize LCD display UART
	UartPsLCDinit(LCD_UART_DEVICE_ID);
#endif
	// initialize I2C system
	SC_I2C_Init(IIC_DEVICE_ID);
	// configure the PCA9539 as input on both ports
	config_pca9539(PCA9539_INPUT,PCA9539_INPUT);
	// Initialize the interrupt subsystem
	fabric_interrupt_init();
	// initialize XZDMA system
	XZDma_Init(&ZDma, ZDMA_DEVICE_ID);
	// Connect to the interrupt controller.
	SetupXZDmaInterruptSystem_example(&xInterruptController, &ZDma,  ZDMA_INTR_DEVICE_ID);
	//set the system frequency to 60Hz to start with
	set_system_frequency(F60HZ);
	Xil_In32(ALARM_MEMORY_ADDR);
	// set the voltage and current factors
	dummy_set_sensor_factor();
	// set the alarm limits
	dummy_set_alarm_limits(&alarm_limit_value);   // remove or rename when we get real limits
	set_alarm_limits(&alarm_limit_value);
	// change baud rate to 115200
//	set_display_baud();  //not working
	// write the function select LED screen
	display_function_select();
	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;  // no keys
	sc_des1_inst.alarm_data->keypad_value_reg.bits.keyBValue = 1;  //B key puts statemachine in\\\\\ function screen
	standalone_display();
	// initialize pingpong buffers
	currentBuffer = 0;
	currentBufferPtr = 0;
	// initialize lwIP operation
#ifdef NETWORKING
	currentServer.messageID = 0;
	tcp_connection_status = lwip_main_init();
#endif

	// set the timestamp based on time-of-day (or explicitly if bare-metal)
#ifdef PETALINUX
	set_timestamp(sc_des1_inst);
#else
	set_timestamp_direct(0,0,0);
#endif
	contactor_setting.reg = 0;  // all contactors engaged (no shedding)
	set_contactor(contactor_setting);

	fabric_interrupt_event = 0;

	/************************************************
	 *
	 * PIO operation test of hardware
	 */
	//	pio_voltage();
	//	xdma_test();

	// hit start
	local_control_reg.reg = 0;
	local_control_reg.bits.dataCollectEnable = 1;
	local_control_reg.bits.streamCollectEnable = 0;
	local_control_reg.bits.greenLEDEnable = 1;
	local_control_reg.bits.interruptEnable = 0;
	local_control_reg.bits.streamIntEnable = 0;
	local_control_reg.bits.snapShotIntEnable = 0;
	local_control_reg.bits.alarmInterruptEnable = 0;  // disable alarm interrupts (and keypad)
	local_control_reg.bits.freq60_50 = F60HZ;
	local_control_reg.bits.dcBiasEnable = 1;
	set_control_reg(&local_control_reg);


	//enable interrupts in the AXIS/AXI fifo
	Xil_Out32(FIFO_REG_SRR_ADDR,0x000000A5); // reset the Stream FIFO
	Xil_Out32(FIFO_REG_IER_ADDR, 0x00100000); // Set the IER for Recieve Fifo Programmable Full (PG080 page 22)
	Xil_Out32(FIFO_REG_ISR_ADDR, 0xFFF80000); // Clear the ISR register (PG080 page 22)
	local_control_reg.bits.streamIntEnable = 1;     /// enable stream interrupts
	local_control_reg.bits.streamCollectEnable = 0;
	local_control_reg.bits.interruptEnable = 1;
	local_control_reg.bits.snapShotIntEnable = 1;
	local_control_reg.bits.alarmInterruptEnable = 1;
	set_control_reg(&local_control_reg);


	while(1){  // forever run standalone design listen on network
		if(fabric_interrupt_event == 0 && tcp_connection_status == 0){  // wait for interrupt to wake us up, otherwise do networking stuff
#ifdef NETWORKING
			if (TcpFastTmrFlag) {
				tcp_fasttmr();
				TcpFastTmrFlag = 0;
			}
			if (TcpSlowTmrFlag) {
				tcp_slowtmr();
				TcpSlowTmrFlag = 0;
			}
			xemacif_input(echo_netif);
			transfer_data();
			if(heartbeat == 0 && tcp_connection_status == 0) {  // tcp_connection_status == 0 includes case when PHY goes down
				heartbeat++;
				responseParser( HB_MSG, 0, &currentServer );
				xil_printf("sending heartbeat\r\n");
			} else if(heartbeat == HEARTBEAT_CYCLE_COUNT) {
				heartbeat = 0;
			} else {
				heartbeat++;
			}
#endif
		} else if (fabric_interrupt_event == 1) {

			// reset the interrupt flag
			fabric_interrupt_event = 0;

			if(voltage_interrupt_event.reg != 0 || current_interrupt_event.reg != 0 || frequency_interrupt_event.reg != 0){
				get_alarm_data();
			}
			// voltage/circuit breaker/contactor interrupt [NOT sure what to do, print out results go to Voltage display]
//			if((sc_des1_inst.alarm_data->volt_interrupt_reg.reg & ~voltage_interrupt.reg) != 0) {  // Interrupt clear for frequency/phase
//				voltage_event_clear_operation();
//			}
			if(voltage_interrupt_event.reg != 0) {  //Interrupt SET event voltage
				voltage_event_set_operation();
			}
			// frequency/phase interrupt  [NOT SURE WHAT TO DO, just printing the results and goto the LCD display]
//			if((sc_des1_inst.alarm_data->freq_interrupt_reg.reg & ~frequency_interrupt.reg) != 0) {  // Interrupt clear for frequency/phase
//				frequency_event_clear_operation();
//			}
			if(frequency_interrupt_event.reg != 0) {  //Interrupt SET event frequency phase
				frequency_event_set_operation();
			}
			// current interrupt
			//**********NOTE*****************
			// In the event of a current interrupt on any downstream feed, we automatically disengage the contactor
			// for that downstream feed provided #define AUTOSHED_FEATURE
//			if((sc_des1_inst.alarm_data->current_interrupt_reg.reg & ~current_interrupt.reg) != 0) {  // Interrupt clear
//				current_event_clear_operation();
//			}
			if(current_interrupt_event.reg != 0) {  //Interrupt SET event
				current_event_set_operation();
			}

			// keypad interupt
			if(keypad_interrupt.reg != 0 ){
				keypad_event_operation();

			}
			// stream interrupt
			if(stream_interrupt.reg != 0){
				stream_snapshot_event_operation();
			}
			// update the sc_des1_inst
			get_interrupt_status();
			// if we have an alarm, send a message.  This needs to happen after we cut contactors to keep the CB from tripping
			if(voltage_interrupt_event.reg != 0 || current_interrupt_event.reg != 0 || frequency_interrupt_event.reg != 0){
				if(firstAlarm) {
					firstAlarm = false;
				} else {
					GenerateAlarmMessage();
					responseParser(ALARM_RESPONSE_MSG, 1, &currentServer);
				}
			}

			// re-enable interrupts
			local_control_reg.reg = Xil_In32(CONTROL_REG_ADDR);
			local_control_reg.bits.interruptEnable = 1;
			set_control_reg(&local_control_reg);
		}
		else if(tcp_connection_status < 0) {
			tcp_connection_status--;
			if(abs(tcp_connection_status) > TCP_RECONNECT_COUNT){
				pcb = tcp_new_ip_type(IPADDR_TYPE_ANY);
				if (!pcb) {
					xil_printf("Error creating PCB. Out of Memory\n\r");
					tcp_connection_status = -1;
				}
				else {
					ip_addr_t remote_addr;
					inet_aton(CONTROLLER_NETWORK_ADDRESS, &remote_addr);
					tcp_connection_status = tcp_connect(pcb, &remote_addr, 8080, accept_callback);
					xil_printf("Reconnecting TCP status: %d\r\n", tcp_connection_status);
					if (tcp_connection_status) {
						xil_printf("Error on tcp_connect: %d\r\n", tcp_connection_status);
						tcp_close(pcb);
					} else {
						heartbeat = 0;
						currentServer.messageID = 0;
					}
				}
			}
		}

	}
	//	close_platform();
	return 0;
}

extern XZDma ZDma;		/**<Instance of the ZDMA Device */
extern INTC Intc;	/**< XIntc Instance */

int wordsInFifo;
unsigned long long copyRealTimeDataCount = 0;

int copyRealTimeData(){
	// make sure we have at least 512 words in the hardware buffer
	wordsInFifo = Xil_In32(FIFO_REG_RDFO_ADDR);
//#ifdef DEBUG
	if(wordsInFifo > (RT_FIFO_SIZE - 1025)) {xil_printf("RT Overflow %d\r\n",wordsInFifo);}
//#endif
	//	while(wordsInFifo > TRANSFERSIZE *2 + 36){  // linear mode
	while(wordsInFifo > TRANSFERSIZE + 36){
		copyRealTimeDataCount++;
		switch(currentBuffer) {
		case 0:
			XDMA_Transfer(&ZDma, (u32*)&realTimeDataA[currentBufferPtr]);
			//		currentBufferPtr += XDMA_Tranfer_example(&ZDma,&realTimeDataA[currentBufferPtr],currentBufferPtr);
			break;
		case 1:
			XDMA_Transfer(&ZDma, (u32*)&realTimeDataB[currentBufferPtr]);
			//		currentBufferPtr += XDMA_Tranfer_example(&ZDma,&realTimeDataB[currentBufferPtr],currentBufferPtr);
			break;
		case 2:
			XDMA_Transfer(&ZDma, (u32*)&realTimeDataC[currentBufferPtr]);
			//		currentBufferPtr += XDMA_Tranfer_example(&ZDma,&realTimeDataC[currentBufferPtr],currentBufferPtr);
			break;
		}
		currentBufferPtr += TRANSFERSIZE;
		if(currentBufferPtr >= EIGHT_SECONDS_DATA){
			if(++currentBuffer >= NUMBUFFERS) {
				currentBuffer = 0;
			}
			currentBufferPtr = 0;
		}
		wordsInFifo = Xil_In32(FIFO_REG_RDFO_ADDR);
	}


	return XST_SUCCESS;
}
//This is a dummy function to set the sensor voltage and current factors.
//This will be replaced by (likely) a read from a file that is IPDU specific
//or (unlikely) a set of values that are universal
// values are based on measurements taken with POC IPDU for resistive test fixture base on 00000003 FPGA design
void dummy_set_sensor_factor() {
	sc_des1_inst.sensor_factor->volt_factor_l1 = 77; // assume 120(RMS) peak = 169V reads 3791 (limit of AMC1200 input of +/- 250mV) ADC  will not saturate
	sc_des1_inst.sensor_factor->volt_factor_l2 = 77; // assume 120(RMS) peak = 169V reads 3642 (limit of AMC1200 input of +/- 250mV) ADC will not saturate
	sc_des1_inst.sensor_factor->volt_factor_l3 = 77; // assume 120(RMS) peak = 169V reads 3993 (limit of AMC1200 input of +/- 250mV) ADC will not saturate
	sc_des1_inst.sensor_factor->current_main_factor_l1 = 108; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_main_factor_l2 = 108; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_main_factor_l3 = 108; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_main_factor_n = 108; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_BYP_factor_l1 = 108; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_BYP_factor_l2 = 108; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_BYP_factor_l3 = 108; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_BYP_factor_n = 108; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60A_factor_l1 = 161; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60A_factor_l2 = 161; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60A_factor_l3 = 161; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60A_factor_n = 161; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60B_factor_l1 = 161; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60B_factor_l2 = 161; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60B_factor_l3 = 161; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60B_factor_n = 161; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40A_factor_l1 = 161; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40A_factor_l2 = 161; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40A_factor_l3 = 161; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40A_factor_n = 161; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40B_factor_l1 = 161; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40B_factor_l2 = 161; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40B_factor_l3 = 161; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40B_factor_n = 161; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_20A_factor_l1 = 232; // assume 20A(RMS) peak = 28A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_20A_factor_n = 232; // assume 20A(RMS) peak = 28A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_20B_factor_l2 = 232; // assume 20A(RMS) peak = 28A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_20B_factor_n = 232; // assume 20A(RMS) peak = 28A reads 16384 (half deflection of ADC

}

void dummy_set_alarm_limits(ALARM_LIMIT_T *alarm_limit_value){
	alarm_limit_value->main_l1_volt_max_reg = pow(140.0 * sc_des1_inst.sensor_factor->volt_factor_l1,2);  //140V max
	alarm_limit_value->main_l1_volt_min_reg = pow(96.0 * sc_des1_inst.sensor_factor->volt_factor_l1,2);  //960V max
	alarm_limit_value->main_l2_volt_max_reg = pow(140.0 * sc_des1_inst.sensor_factor->volt_factor_l2,2);  //140V max
	alarm_limit_value->main_l2_volt_min_reg = pow(96.0 * sc_des1_inst.sensor_factor->volt_factor_l2,2);  //960V max
	alarm_limit_value->main_l3_volt_max_reg = pow(140.0 * sc_des1_inst.sensor_factor->volt_factor_l3,2);  //140V max
	alarm_limit_value->main_l3_volt_min_reg = pow(96.0 * sc_des1_inst.sensor_factor->volt_factor_l3,2);  //960V max

	get_control_reg();

	if(sc_des1_inst.alarm_data->control_reg.bits.freq60_50 == F60HZ){  // 60Hz
		alarm_limit_value->main_freq_max_reg = 0xFFFFFFFF;
		alarm_limit_value->main_freq_min_reg = 0;
	} else {
		alarm_limit_value->main_freq_max_reg = 0xFFFFFFFF;
		alarm_limit_value->main_freq_min_reg = 0;

	}
	if(sc_des1_inst.alarm_data->control_reg.bits.freq60_50 == F60HZ){  // 60Hz
		alarm_limit_value->main_phase_max_reg = 0xFFFFFFFF;
		alarm_limit_value->main_phase_min_reg = 0;
	} else {
		alarm_limit_value->main_phase_max_reg = 0xFFFFFFFF;
		alarm_limit_value->main_phase_min_reg = 0;

	}
	alarm_limit_value->main_l1_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->main_l2_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->main_l3_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->main_n_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f60a_l1_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f60a_l2_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f60a_l3_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f60a_n_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f60b_l1_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f60b_l2_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f60b_l3_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f60b_n_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f40a_l1_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f40a_l2_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f40a_l3_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f40a_n_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f40b_l1_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f40b_l2_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f40b_l3_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f40b_n_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->f20a_l1_amp_max_reg = /*0x3596F593; //~600%*/ 50000000;
	alarm_limit_value->f20a_n_amp_max_reg = /*0x3596F593; //~600%*/  50000000;
	alarm_limit_value->f20b_l2_amp_max_reg = /*0x3596F593; //~600%*/ 50000000;
	alarm_limit_value->f20b_n_amp_max_reg = /*0x3596F593; //~600%*/  50000000;
	alarm_limit_value->fbyp_l1_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->fbyp_l2_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->fbyp_l3_amp_max_reg = 0x3596F593; //~600%
	alarm_limit_value->fbyp_n_amp_max_reg = 0x3596F593; //~600%


}

void voltage_event_clear_operation(){
	xil_printf("Voltage Interrupt clear event %08x %08x\r\n", sc_des1_inst.alarm_data->volt_interrupt_reg.reg , voltage_interrupt_event.reg);
	// goto Voltage Snapshot display on the LCD display
	//	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	//	sc_des1_inst.alarm_data->keypad_value_reg.bits.keyBValue = 1;
	//	standalone_display();
	//	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	//	sc_des1_inst.alarm_data->keypad_value_reg.bits.key1Value = 1;
	//	standalone_display();
}

void voltage_event_set_operation(){
	xil_printf("Voltage status %08x %08x\r\n", VoltageStatus.reg , previousVoltageStatus.reg);
	xil_printf("Voltage Interrupt event %08x \r\n", voltage_interrupt_event.reg);
	printf("VOLTAGE Interrupt data:  L1: %f  L2: %f  L3: %f\r\n",
			RMS_calculation(sc_des1_inst.alarm_data->main_l1_volt_reg,sc_des1_inst.sensor_factor->volt_factor_l1),
			RMS_calculation(sc_des1_inst.alarm_data->main_l2_volt_reg,sc_des1_inst.sensor_factor->volt_factor_l2),
			RMS_calculation(sc_des1_inst.alarm_data->main_l3_volt_reg,sc_des1_inst.sensor_factor->volt_factor_l3));
	xil_printf("Circuit Breakers state: MAIN %d  F60A %d F60B %d F40A %d F40B %d F20A %d F20B %d\r\n",
			sc_des1_inst.alarm_data->circuit_breaker_status_reg.bits.mainCircuitBreaker ? 1 : 0,
			sc_des1_inst.alarm_data->circuit_breaker_status_reg.bits.f60ACircuitBreaker ? 1 : 0,
		    sc_des1_inst.alarm_data->circuit_breaker_status_reg.bits.f60BCircuitBreaker ? 1 : 0,
		    sc_des1_inst.alarm_data->circuit_breaker_status_reg.bits.f40ACircuitBreaker ? 1 : 0,
		    sc_des1_inst.alarm_data->circuit_breaker_status_reg.bits.f40BCircuitBreaker ? 1 : 0,
		    sc_des1_inst.alarm_data->circuit_breaker_status_reg.bits.f20ACircuitBreaker ? 1 : 0,
	        sc_des1_inst.alarm_data->circuit_breaker_status_reg.bits.f20BCircuitBreaker ? 1 : 0);
	xil_printf("Contactor state: F60A %d F60B %d F40A %d F40B %d F20A %d F20B %d BYP %d\r\n",
			sc_des1_inst.alarm_data->contactor_disengage_reg.bits.f60AContactor ? 1 : 0,
			sc_des1_inst.alarm_data->contactor_disengage_reg.bits.f60BContactor ? 1 : 0,
			sc_des1_inst.alarm_data->contactor_disengage_reg.bits.f40AContactor ? 1 : 0,
			sc_des1_inst.alarm_data->contactor_disengage_reg.bits.f40BContactor ? 1 : 0,
			sc_des1_inst.alarm_data->contactor_disengage_reg.bits.f20AContactor ? 1 : 0,
     		sc_des1_inst.alarm_data->contactor_disengage_reg.bits.f20BContactor ? 1 : 0,
			sc_des1_inst.alarm_data->contactor_disengage_reg.bits.bypassContactor ? 1 : 0);

	// goto Voltage Snapshot display on the LCD display
	//	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	//	sc_des1_inst.alarm_data->keypad_value_reg.bits.keyBValue = 1;
	//	standalone_display();
	//	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	//	sc_des1_inst.alarm_data->keypad_value_reg.bits.key3Value = 1;
	//	standalone_display();
}

void frequency_event_clear_operation(){
	xil_printf("FREQUENCY/PHASE Interrupt clear event %08x %08x\r\n", sc_des1_inst.alarm_data->freq_interrupt_reg.reg , frequency_interrupt_event.reg);
	// goto Frequency Snapshot display on the LCD display
	//	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	//	sc_des1_inst.alarm_data->keypad_value_reg.bits.keyBValue = 1;
	//	standalone_display();
	//	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	//	sc_des1_inst.alarm_data->keypad_value_reg.bits.key3Value = 1;
	//	standalone_display();
}

void frequency_event_set_operation(){
	double freql1 = FREQ_calculation(sc_des1_inst.alarm_data->main_l1_freq_reg);
	double freql2 = FREQ_calculation(sc_des1_inst.alarm_data->main_l2_freq_reg);
	double freql3 = FREQ_calculation(sc_des1_inst.alarm_data->main_l3_freq_reg);
	double phasel1 = PHASE_calculation(sc_des1_inst.alarm_data->main_l1_freq_reg, sc_des1_inst.alarm_data->main_l1_phase_reg);
	double phasel2 = PHASE_calculation(sc_des1_inst.alarm_data->main_l2_freq_reg, sc_des1_inst.alarm_data->main_l2_phase_reg);
	double phasel3 = PHASE_calculation(sc_des1_inst.alarm_data->main_l3_freq_reg, sc_des1_inst.alarm_data->main_l3_phase_reg);

	xil_printf("FREQUENCY/PHASE status %08x %08x\r\n", FreqStatus.reg , previousFreqStatus.reg );
	xil_printf("FREQUENCY/PHASE Interrupt event %08x \r\n", frequency_interrupt_event.reg );
	printf("FREQUENCY Interrupt data: FREQ L1: %f  FREQ L2: %f  FREQ L3: %f\r\n", freql1, freql2, freql3);
	printf("PHASE Interrupt data:     PHAS L1: %f  PHAS L2: %f  PHAS L3: %f\r\n", phasel1, phasel2, phasel3);
	// goto Frequency Snapshot display on the LCD display
	//	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	//	sc_des1_inst.alarm_data->keypad_value_reg.bits.keyBValue = 1;
	//	standalone_display();
	//	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	//	sc_des1_inst.alarm_data->keypad_value_reg.bits.key3Value = 1;
	//	standalone_display();
}

void current_event_clear_operation(){
	xil_printf("CURRENT Interrupt clear event %08x %08x\r\n", sc_des1_inst.alarm_data->current_interrupt_reg.reg , current_interrupt_event.reg);
/* ******DEBUG interrupts
	u32_t main_n_amp_max_reg_tmp;
	u32_t main_n_amp_reg_tmp;
//	u32_t current_interrupt_reg_tmp;
//	current_interrupt_reg_tmp = Xil_In32(CURRENT_INTERRUPT_ADDR);
	main_n_amp_max_reg_tmp = Xil_In32(MAIN_N_AMP_MAX_ADDR);
	main_n_amp_reg_tmp = Xil_In32(MAIN_N_AMP_ADDR);
	xil_printf("Neutral error:  reg: 0x%08x, limit: 0x%08x\r\n",main_n_amp_reg_tmp,main_n_amp_max_reg_tmp);
*/
}

//**********NOTE*****************
// In the event of a current interrupt on any downstream feed, we automatically disengage the contactor
// for that downstream feed

void current_event_set_operation(){
	xil_printf("CURRENT status %08x %08x\r\n", CurrentStatus.reg , previousCurrentStatus.reg);
	xil_printf("CURRENT Interrupt event %08x \r\n", current_interrupt_event.reg );
/*   ******DEBUG interrupts
	u32_t main_n_amp_max_reg_tmp;
	u32_t main_n_amp_reg_tmp;
//	u32_t current_interrupt_reg_tmp;
//	current_interrupt_reg_tmp = Xil_In32(CURRENT_INTERRUPT_ADDR);
	main_n_amp_max_reg_tmp = Xil_In32(MAIN_N_AMP_MAX_ADDR);
	main_n_amp_reg_tmp = Xil_In32(MAIN_N_AMP_ADDR);
	xil_printf("Neutral error:  reg: 0x%08x, limit: 0x%08x\r\n",main_n_amp_reg_tmp,main_n_amp_max_reg_tmp);
*/
#define AUTOSHED_FEATURE
#ifdef AUTOSHED_FEATURE
		CONTACTOR_DISENGAGE_T currentContactor;
		currentContactor.reg = sc_des1_inst.alarm_data->contactor_disengage_reg.reg;
#endif
	// bypass alarm
	if(sc_des1_inst.alarm_data->current_interrupt_reg.bits.byp_L1_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.byp_L2_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.byp_L3_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.byp_N_Overcurrent == 1) {
#ifdef AUTOSHED_FEATURE
		currentContactor.bits.bypassContactor = 1;
		set_contactor(currentContactor );
#endif
		printf("BYPASS Overcurrent Alarm:  L1: %f  L2: %f  L3: %f  N: %f\r\n",
				RMS_calculation(sc_des1_inst.alarm_data->fbyp_l1_amp_reg,sc_des1_inst.sensor_factor->current_BYP_factor_l1),
				RMS_calculation(sc_des1_inst.alarm_data->fbyp_l2_amp_reg,sc_des1_inst.sensor_factor->current_BYP_factor_l2),
				RMS_calculation(sc_des1_inst.alarm_data->fbyp_l3_amp_reg,sc_des1_inst.sensor_factor->current_BYP_factor_l3),
				RMS_calculation(sc_des1_inst.alarm_data->fbyp_n_amp_reg,sc_des1_inst.sensor_factor->current_BYP_factor_n));
	}
	// 60A alarm
	if(sc_des1_inst.alarm_data->current_interrupt_reg.bits.f60A_L1_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.f60A_L2_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.f60A_L3_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.f60A_N_Overcurrent == 1) {
#ifdef AUTOSHED_FEATURE
		currentContactor.bits.f60AContactor = 1;
		set_contactor(currentContactor);
#endif
		printf("Feed 60A Overcurrent Alarm:  L1: %f  L2: %f  L3: %f  N: %f\r\n",
				RMS_calculation(sc_des1_inst.alarm_data->f60a_l1_amp_reg,sc_des1_inst.sensor_factor->current_60A_factor_l1),
				RMS_calculation(sc_des1_inst.alarm_data->f60a_l2_amp_reg,sc_des1_inst.sensor_factor->current_60A_factor_l2),
				RMS_calculation(sc_des1_inst.alarm_data->f60a_l3_amp_reg,sc_des1_inst.sensor_factor->current_60A_factor_l3),
				RMS_calculation(sc_des1_inst.alarm_data->f60a_n_amp_reg,sc_des1_inst.sensor_factor->current_60A_factor_n));

	}
	// 60B alarm
	if(sc_des1_inst.alarm_data->current_interrupt_reg.bits.f60B_L1_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.f60B_L2_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.f60B_L3_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.f60B_N_Overcurrent == 1) {
#ifdef AUTOSHED_FEATURE
		currentContactor.bits.f60BContactor = 1;
		set_contactor(currentContactor);
#endif
		printf("Feed 60B Overcurrent Alarm:  L1: %f  L2: %f  L3: %f  N: %f\r\n",
				RMS_calculation(sc_des1_inst.alarm_data->f60b_l1_amp_reg,sc_des1_inst.sensor_factor->current_60B_factor_l1),
				RMS_calculation(sc_des1_inst.alarm_data->f60b_l2_amp_reg,sc_des1_inst.sensor_factor->current_60B_factor_l2),
				RMS_calculation(sc_des1_inst.alarm_data->f60b_l3_amp_reg,sc_des1_inst.sensor_factor->current_60B_factor_l3),
				RMS_calculation(sc_des1_inst.alarm_data->f60b_n_amp_reg,sc_des1_inst.sensor_factor->current_60B_factor_n));

	}
	// 40A alarm
	if(sc_des1_inst.alarm_data->current_interrupt_reg.bits.f40A_L1_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.f40A_L2_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.f40A_L3_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.f40A_N_Overcurrent == 1) {
#ifdef AUTOSHED_FEATURE
		currentContactor.bits.f40AContactor = 1;
		set_contactor(currentContactor);
#endif
		printf("Feed 40A Overcurrent Alarm:  L1: %f  L2: %f  L3: %f  N: %f\r\n",
				RMS_calculation(sc_des1_inst.alarm_data->f40a_l1_amp_reg,sc_des1_inst.sensor_factor->current_40A_factor_l1),
				RMS_calculation(sc_des1_inst.alarm_data->f40a_l2_amp_reg,sc_des1_inst.sensor_factor->current_40A_factor_l2),
				RMS_calculation(sc_des1_inst.alarm_data->f40a_l3_amp_reg,sc_des1_inst.sensor_factor->current_40A_factor_l3),
				RMS_calculation(sc_des1_inst.alarm_data->f40a_n_amp_reg,sc_des1_inst.sensor_factor->current_40A_factor_n));

	}
	// 40B alarm
	if(sc_des1_inst.alarm_data->current_interrupt_reg.bits.f40B_L1_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.f40B_L2_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.f40B_L3_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.f40B_N_Overcurrent == 1) {
#ifdef AUTOSHED_FEATURE
		currentContactor.bits.f40BContactor = 1;
		set_contactor(currentContactor);
#endif
		printf("Feed 40B Overcurrent Alarm:  L1: %f  L2: %f  L3: %f  N: %f\r\n",
				RMS_calculation(sc_des1_inst.alarm_data->f40b_l1_amp_reg,sc_des1_inst.sensor_factor->current_40B_factor_l1),
				RMS_calculation(sc_des1_inst.alarm_data->f40b_l2_amp_reg,sc_des1_inst.sensor_factor->current_40B_factor_l2),
				RMS_calculation(sc_des1_inst.alarm_data->f40b_l3_amp_reg,sc_des1_inst.sensor_factor->current_40B_factor_l3),
				RMS_calculation(sc_des1_inst.alarm_data->f40b_n_amp_reg,sc_des1_inst.sensor_factor->current_40B_factor_n));

	}
	// 20A alarm
	if(sc_des1_inst.alarm_data->current_interrupt_reg.bits.f20A_L1_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.f20A_N_Overcurrent == 1) {
#ifdef AUTOSHED_FEATURE
		currentContactor.bits.f20AContactor = 1;
		set_contactor(currentContactor);
#endif
		printf("Feed 20A Overcurrent Alarm:  L1: %f  N: %f\r\n",
				RMS_calculation(sc_des1_inst.alarm_data->f20a_l1_amp_reg,sc_des1_inst.sensor_factor->current_20A_factor_l1),
				RMS_calculation(sc_des1_inst.alarm_data->f20a_n_amp_reg,sc_des1_inst.sensor_factor->current_20A_factor_n));

	}
	// 20B alarm
	if(sc_des1_inst.alarm_data->current_interrupt_reg.bits.f20B_L2_Overcurrent == 1 ||
			sc_des1_inst.alarm_data->current_interrupt_reg.bits.f20B_N_Overcurrent == 1) {
#ifdef AUTOSHED_FEATURE
		currentContactor.bits.f20BContactor = 1;
		set_contactor(currentContactor);
#endif
		printf("Feed 20B Overcurrent Alarm:  L1: %f  N: %f\r\n",
				RMS_calculation(sc_des1_inst.alarm_data->f20b_l2_amp_reg,sc_des1_inst.sensor_factor->current_20B_factor_l2),
				RMS_calculation(sc_des1_inst.alarm_data->f20b_n_amp_reg,sc_des1_inst.sensor_factor->current_20B_factor_n));

	}
}

void keypad_event_operation(){
	get_keypad_values();
	standalone_display();
}
int screen_update_interval = 0;

void stream_snapshot_event_operation(){
	if(stream_interrupt.bits.streamFifoInterrupt == 1) {
		copyRealTimeData();
		Xil_Out32(FIFO_REG_ISR_ADDR, 0xFFF80000); // Clear the ISR register (PG080 page 22)
		Xil_Out32(FIFO_REG_IER_ADDR, 0x00100000); // Set the IER for Recieve Fifo Programmable Full (PG080 page 22)
	}
	if(stream_interrupt.bits.snapShotUpdateInterrupt == 1) {  // changed V22 to allow simultaneous interrupts
		get_snapshot_data();
		get_temperature();
		get_pca9539_status();  // FIXME on DISPLAY
		get_FreqPhase_data();
		get_timestamp();

		screen_update_interval++;
		if(screen_update_interval > LCD_UPDATE_INTERVAL){
			update_LCD_display();
			screen_update_interval = 0;
		}
	}
}

alarmData  testAlarmData;


void GenerateAlarmMessage() {
	memset((void *)&testAlarmData,0,sizeof(testAlarmData));
// voltage alarm event
	if(sc_des1_inst.alarm_data->volt_int_event_reg.bits.circuitBreakerTripEvent)
		testAlarmData.alarmType.reg  |= CIRCUIT_BREAKER_ALARM_TYPE;
	if(sc_des1_inst.alarm_data->volt_int_event_reg.bits.contactorOpenEvent)
		testAlarmData.alarmType.reg  |= CONTACTOR_ALARM_TYPE;
	if(sc_des1_inst.alarm_data->volt_int_event_reg.reg & 0x3F)  // any of the L1/L2/L3 over or under voltages
		testAlarmData.alarmType.reg  |= VOLTAGE_ALARM_TYPE;
	testAlarmData.voltageStatus.reg = sc_des1_inst.alarm_data->volt_interrupt_reg.reg;
//	testAlarmData.voltageStatus.reg |= (sc_des1_inst.alarm_data->circuit_breaker_status_reg.reg & CIRCUITBREAKER_MASK) <<CIRCUITBREAKER_ALARM_SHIFT;
//	testAlarmData.voltageStatus.reg |= (sc_des1_inst.alarm_data->contactor_disengage_reg.reg & CONTACTOR_MASK) << CONTACTOR_ALARM_SHIFT;   // should we remove bypass contactor
	testAlarmData.voltageEvent.reg = sc_des1_inst.alarm_data->volt_int_event_reg.reg;
//	testAlarmData.voltageEvent.reg |= ((previousCircuitBreakerStatus.reg ^sc_des1_inst.alarm_data->circuit_breaker_status_reg.reg) & CIRCUITBREAKER_MASK) << CIRCUITBREAKER_ALARM_SHIFT;
//	testAlarmData.voltageEvent.reg |= ((previousContactorStatus.reg ^ sc_des1_inst.alarm_data->contactor_disengage_reg.reg) & CONTACTOR_MASK) << CONTACTOR_ALARM_SHIFT;   // should we remove bypass contactor
// frequency/phase alarm event
	if(sc_des1_inst.alarm_data->freq_int_event_reg.reg & 0x3F)  // any of the L1/L2/L3 over/under frequency event
		testAlarmData.alarmType.reg  |= FREQUENCY_ALARM_TYPE;
	if(sc_des1_inst.alarm_data->freq_int_event_reg.reg & 0x3F00)  // any of the L1/L2/L3 over/under phase event
		testAlarmData.alarmType.reg  |= PHASE_ALARM_TYPE;
	testAlarmData.freqPhaseStatus.reg = sc_des1_inst.alarm_data->freq_interrupt_reg.reg;
	testAlarmData.freqPhaseEvent.reg = sc_des1_inst.alarm_data->freq_int_event_reg.reg;
// frequency/phase alarm event
	if(sc_des1_inst.alarm_data->current_int_event_reg.reg)  // any of the L1/L2/L3 over/under frequency event
		testAlarmData.alarmType.reg  |= CURRENT_ALARM_TYPE;
	testAlarmData.ampStatus.reg = sc_des1_inst.alarm_data->current_interrupt_reg.reg;
	testAlarmData.ampEvent.reg = sc_des1_inst.alarm_data->current_int_event_reg.reg;
	memcpy(&testAlarmData.oneCycleSnapshotData.main_l1_volt_reg,&sc_des1_inst.alarm_data->main_l1_volt_reg,sizeof(ONE_CYCLE_SNAPSHOT_T) - sizeof(TIMESTAMP_REGISTER_T));
	memcpy(&testAlarmData.oneCycleSnapshotData.timestamp_seconds_msb,&sc_des1_inst.alarm_data->timestamp_seconds_msb,sizeof(TIMESTAMP_REGISTER_T));

}


