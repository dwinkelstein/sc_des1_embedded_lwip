/*
 * SC_PIO.c
 *
 *  Created on: Feb 17, 2021
 *      Author: DanWinkelstein
 */

#include "SC_des1.h"

extern VOLT_INT_EVENT_T voltage_interrupt_event;
extern FREQ_INT_EVENT_T frequency_interrupt_event;
extern KEYPAD_INTERRUPT_T keypad_interrupt;
extern CURRENT_INT_EVENT_T current_interrupt_event;
extern STREAM_INTERRUPT_T stream_interrupt;
extern VOLT_INTERRUPT_T VoltageStatus;
extern FREQ_INTERRUPT_T FreqStatus;
extern CURRENT_INTERRUPT_T CurrentStatus;

extern SC_DES1_T sc_des1_inst;

extern CONTACTOR_DISENGAGE_T previousContactorStatus;
extern CIRCUIT_BREAKER_STATUS_T previousCircuitBreakerStatus;
extern VOLT_INTERRUPT_T VoltageStatus;
extern FREQ_INTERRUPT_T FreqStatus;
extern CURRENT_INTERRUPT_T CurrentStatus;
extern VOLT_INTERRUPT_T previousVoltageStatus;
extern FREQ_INTERRUPT_T previousFreqStatus;
extern CURRENT_INTERRUPT_T previousCurrentStatus;

// copy everyting in the snapshot memory into the sc_des1_inst instance
// uses Memcpy as a PIO operation.  (if this takes too long we will do DMA)
void get_snapshot_data() {  // get all the data in the snapshot memory and put it into the datastructure
//	Xil_MemCpy(sc_des1_inst.snapshot_data, SNAPSHOT_MEMORY_ADDR, SNAPSHOT_MEMORY_SIZE);
	sc_des1_inst.snapshot_data->snapshot_volt_l1 = Xil_In32(SNAPSHOT_VOLT_L1_ADDR);
	sc_des1_inst.snapshot_data->snapshot_volt_l2 = Xil_In32(SNAPSHOT_VOLT_L2_ADDR);
	sc_des1_inst.snapshot_data->snapshot_volt_l3 = Xil_In32(SNAPSHOT_VOLT_L3_ADDR);
	sc_des1_inst.snapshot_data->snapshot_noise = Xil_In32(SNAPSHOT_NOISE_ADDR);

	sc_des1_inst.snapshot_data->snapshot_amp_main_l1 = Xil_In32(SNAPSHOT_AMP_MAIN_L1);
	sc_des1_inst.snapshot_data->snapshot_amp_main_l2 = Xil_In32(SNAPSHOT_AMP_MAIN_L2);
	sc_des1_inst.snapshot_data->snapshot_amp_main_l3 = Xil_In32(SNAPSHOT_AMP_MAIN_L3);
	sc_des1_inst.snapshot_data->snapshot_amp_main_n = Xil_In32(SNAPSHOT_AMP_MAIN_N);
	sc_des1_inst.snapshot_data->snapshot_pow_main_l1 = Xil_In32(SNAPSHOT_POW_MAIN_L1);
	sc_des1_inst.snapshot_data->snapshot_pow_main_l2 = Xil_In32(SNAPSHOT_POW_MAIN_L2);
	sc_des1_inst.snapshot_data->snapshot_pow_main_l3 = Xil_In32(SNAPSHOT_POW_MAIN_L3);

	sc_des1_inst.snapshot_data->snapshot_amp_f60a_l1 = Xil_In32(SNAPSHOT_AMP_F60A_L1);
	sc_des1_inst.snapshot_data->snapshot_amp_f60a_l2 = Xil_In32(SNAPSHOT_AMP_F60A_L2);
	sc_des1_inst.snapshot_data->snapshot_amp_f60a_l3 = Xil_In32(SNAPSHOT_AMP_F60A_L3);
	sc_des1_inst.snapshot_data->snapshot_amp_f60a_n = Xil_In32(SNAPSHOT_AMP_F60A_N);
	sc_des1_inst.snapshot_data->snapshot_pow_f60a_l1 = Xil_In32(SNAPSHOT_POW_F60A_L1);
	sc_des1_inst.snapshot_data->snapshot_pow_f60a_l2 = Xil_In32(SNAPSHOT_POW_F60A_L2);
	sc_des1_inst.snapshot_data->snapshot_pow_f60a_l3 = Xil_In32(SNAPSHOT_POW_F60A_L3);

	sc_des1_inst.snapshot_data->snapshot_amp_f60b_l1 = Xil_In32(SNAPSHOT_AMP_F60B_L1);
	sc_des1_inst.snapshot_data->snapshot_amp_f60b_l2 = Xil_In32(SNAPSHOT_AMP_F60B_L2);
	sc_des1_inst.snapshot_data->snapshot_amp_f60b_l3 = Xil_In32(SNAPSHOT_AMP_F60B_L3);
	sc_des1_inst.snapshot_data->snapshot_amp_f60b_n = Xil_In32(SNAPSHOT_AMP_F60B_N);
	sc_des1_inst.snapshot_data->snapshot_pow_f60b_l1 = Xil_In32(SNAPSHOT_POW_F60B_L1);
	sc_des1_inst.snapshot_data->snapshot_pow_f60b_l2 = Xil_In32(SNAPSHOT_POW_F60B_L2);
	sc_des1_inst.snapshot_data->snapshot_pow_f60b_l3 = Xil_In32(SNAPSHOT_POW_F60B_L3);

	sc_des1_inst.snapshot_data->snapshot_amp_f40a_l1 = Xil_In32(SNAPSHOT_AMP_F40A_L1);
	sc_des1_inst.snapshot_data->snapshot_amp_f40a_l2 = Xil_In32(SNAPSHOT_AMP_F40A_L2);
	sc_des1_inst.snapshot_data->snapshot_amp_f40a_l3 = Xil_In32(SNAPSHOT_AMP_F40A_L3);
	sc_des1_inst.snapshot_data->snapshot_amp_f40a_n = Xil_In32(SNAPSHOT_AMP_F40A_N);
	sc_des1_inst.snapshot_data->snapshot_pow_f40a_l1 = Xil_In32(SNAPSHOT_POW_F40A_L1);
	sc_des1_inst.snapshot_data->snapshot_pow_f40a_l2 = Xil_In32(SNAPSHOT_POW_F40A_L2);
	sc_des1_inst.snapshot_data->snapshot_pow_f40a_l3 = Xil_In32(SNAPSHOT_POW_F40A_L3);

	sc_des1_inst.snapshot_data->snapshot_amp_f40b_l1 = Xil_In32(SNAPSHOT_AMP_F40B_L1);
	sc_des1_inst.snapshot_data->snapshot_amp_f40b_l2 = Xil_In32(SNAPSHOT_AMP_F40B_L2);
	sc_des1_inst.snapshot_data->snapshot_amp_f40b_l3 = Xil_In32(SNAPSHOT_AMP_F40B_L3);
	sc_des1_inst.snapshot_data->snapshot_amp_f40b_n = Xil_In32(SNAPSHOT_AMP_F40B_N);
	sc_des1_inst.snapshot_data->snapshot_pow_f40b_l1 = Xil_In32(SNAPSHOT_POW_F40B_L1);
	sc_des1_inst.snapshot_data->snapshot_pow_f40b_l2 = Xil_In32(SNAPSHOT_POW_F40B_L2);
	sc_des1_inst.snapshot_data->snapshot_pow_f40b_l3 = Xil_In32(SNAPSHOT_POW_F40B_L3);

	sc_des1_inst.snapshot_data->snapshot_amp_f20a_l1 = Xil_In32(SNAPSHOT_AMP_F20A_L1);
	sc_des1_inst.snapshot_data->snapshot_amp_f20a_n = Xil_In32(SNAPSHOT_AMP_F20A_N);
	sc_des1_inst.snapshot_data->snapshot_pow_f20a_l1 = Xil_In32(SNAPSHOT_POW_F20A_L1);

	sc_des1_inst.snapshot_data->snapshot_amp_f20b_l2 = Xil_In32(SNAPSHOT_AMP_F20B_L2);
	sc_des1_inst.snapshot_data->snapshot_amp_f20b_n = Xil_In32(SNAPSHOT_AMP_F20B_N);
	sc_des1_inst.snapshot_data->snapshot_pow_f20b_l2 = Xil_In32(SNAPSHOT_POW_F20B_L2);

	sc_des1_inst.snapshot_data->snapshot_amp_byp_l1 = Xil_In32(SNAPSHOT_AMP_FBYP_L1);
	sc_des1_inst.snapshot_data->snapshot_amp_byp_l2 = Xil_In32(SNAPSHOT_AMP_FBYP_L2);
	sc_des1_inst.snapshot_data->snapshot_amp_byp_l3 = Xil_In32(SNAPSHOT_AMP_FBYP_L3);
	sc_des1_inst.snapshot_data->snapshot_amp_byp_n = Xil_In32(SNAPSHOT_AMP_FBYP_N);
	sc_des1_inst.snapshot_data->snapshot_pow_byp_l1 = Xil_In32(SNAPSHOT_POW_FBYP_L1);
	sc_des1_inst.snapshot_data->snapshot_pow_byp_l2 = Xil_In32(SNAPSHOT_POW_FBYP_L2);
	sc_des1_inst.snapshot_data->snapshot_pow_byp_l3 = Xil_In32(SNAPSHOT_POW_FBYP_L3);

}


// copy Frequency and phase all the alarm registers into the sc_des1_inst instance
void get_FreqPhase_data() {  // get all the data in the snapshot memory and put it into the datastructure
	sc_des1_inst.alarm_data->main_l1_freq_reg = Xil_In32(MAIN_L1_FREQ_ADDR);
	sc_des1_inst.alarm_data->main_l2_freq_reg = Xil_In32(MAIN_L2_FREQ_ADDR);
	sc_des1_inst.alarm_data->main_l3_freq_reg = Xil_In32(MAIN_L3_FREQ_ADDR);
	sc_des1_inst.alarm_data->main_l1_phase_reg = Xil_In32(MAIN_L1L2_PHASE_ADDR);
	sc_des1_inst.alarm_data->main_l2_phase_reg = Xil_In32(MAIN_L2L3_PHASE_ADDR);
	sc_des1_inst.alarm_data->main_l3_phase_reg = Xil_In32(MAIN_L3L1_PHASE_ADDR);
}

// copy all the alarm registers into the sc_des1_inst instance
// this is from contactor disengage to fbyps_n_amp_reg inclusive [FIXME, needs to be a usable structure]
// uses Memcpy as a PIO operation on 39 words
void get_alarm_data() {  // get all the data in the snapshot memory and put it into the datastructure
	previousContactorStatus.reg = sc_des1_inst.alarm_data->contactor_disengage_reg.reg;
	previousCircuitBreakerStatus.reg = sc_des1_inst.alarm_data->circuit_breaker_status_reg.reg;
	previousVoltageStatus.reg = sc_des1_inst.alarm_data->volt_interrupt_reg.reg;
	previousFreqStatus.reg = sc_des1_inst.alarm_data->freq_interrupt_reg.reg;
	previousCurrentStatus.reg = sc_des1_inst.alarm_data->current_interrupt_reg.reg;
	sc_des1_inst.alarm_data->volt_interrupt_reg.reg = VoltageStatus.reg;
	sc_des1_inst.alarm_data->volt_int_event_reg.reg = voltage_interrupt_event.reg;
	sc_des1_inst.alarm_data->freq_interrupt_reg.reg = FreqStatus.reg;
	sc_des1_inst.alarm_data->freq_int_event_reg.reg = frequency_interrupt_event.reg;
	sc_des1_inst.alarm_data->current_interrupt_reg.reg = CurrentStatus.reg;
	sc_des1_inst.alarm_data->current_int_event_reg.reg = current_interrupt_event.reg;
//	Xil_MemCpy(&sc_des1_inst.alarm_data->contactor_disengage_reg, CONTACTOR_DISENGAGE_ADDR, ALARM_DATA_SIZE);
	sc_des1_inst.alarm_data->contactor_disengage_reg.reg = Xil_In32(CONTACTOR_DISENGAGE_ADDR);
	sc_des1_inst.alarm_data->circuit_breaker_status_reg.reg = Xil_In32(CIRCUIT_BREAKER_STATUS_ADDR);
	sc_des1_inst.alarm_data->main_l1_volt_reg = Xil_In32(MAIN_L1_VOLT_ADDR);
	sc_des1_inst.alarm_data->main_l2_volt_reg = Xil_In32(MAIN_L2_VOLT_ADDR);
	sc_des1_inst.alarm_data->main_l3_volt_reg = Xil_In32(MAIN_L3_VOLT_ADDR);
	sc_des1_inst.alarm_data->main_l1_freq_reg = Xil_In32(MAIN_L1_FREQ_ADDR);
	sc_des1_inst.alarm_data->main_l2_freq_reg = Xil_In32(MAIN_L2_FREQ_ADDR);
	sc_des1_inst.alarm_data->main_l3_freq_reg = Xil_In32(MAIN_L3_FREQ_ADDR);
	sc_des1_inst.alarm_data->main_l1_phase_reg = Xil_In32(MAIN_L1L2_PHASE_ADDR);
	sc_des1_inst.alarm_data->main_l2_phase_reg = Xil_In32(MAIN_L2L3_PHASE_ADDR);
	sc_des1_inst.alarm_data->main_l3_phase_reg = Xil_In32(MAIN_L3L1_PHASE_ADDR);
	sc_des1_inst.alarm_data->main_l1_amp_reg = Xil_In32(MAIN_L1_AMP_ADDR);
	sc_des1_inst.alarm_data->main_l2_amp_reg = Xil_In32(MAIN_L2_AMP_ADDR);
	sc_des1_inst.alarm_data->main_l3_amp_reg = Xil_In32(MAIN_L3_AMP_ADDR);
	sc_des1_inst.alarm_data->main_n_amp_reg = Xil_In32(MAIN_N_AMP_ADDR);
	sc_des1_inst.alarm_data->f60a_l1_amp_reg = Xil_In32(F60A_L1_AMP_ADDR);
	sc_des1_inst.alarm_data->f60a_l2_amp_reg = Xil_In32(F60A_L2_AMP_ADDR);
	sc_des1_inst.alarm_data->f60a_l3_amp_reg = Xil_In32(F60A_L3_AMP_ADDR);
	sc_des1_inst.alarm_data->f60a_n_amp_reg = Xil_In32(F60A_N_AMP_ADDR);
	sc_des1_inst.alarm_data->f60b_l1_amp_reg = Xil_In32(F60B_L1_AMP_ADDR);
	sc_des1_inst.alarm_data->f60b_l2_amp_reg = Xil_In32(F60B_L2_AMP_ADDR);
	sc_des1_inst.alarm_data->f60b_l3_amp_reg = Xil_In32(F60B_L3_AMP_ADDR);
	sc_des1_inst.alarm_data->f60b_n_amp_reg = Xil_In32(F60B_N_AMP_ADDR);
	sc_des1_inst.alarm_data->f40a_l1_amp_reg = Xil_In32(F40A_L1_AMP_ADDR);
	sc_des1_inst.alarm_data->f40a_l2_amp_reg = Xil_In32(F40A_L2_AMP_ADDR);
	sc_des1_inst.alarm_data->f40a_l3_amp_reg = Xil_In32(F40A_L3_AMP_ADDR);
	sc_des1_inst.alarm_data->f40a_n_amp_reg = Xil_In32(F40A_N_AMP_ADDR);
	sc_des1_inst.alarm_data->f40b_l1_amp_reg = Xil_In32(F40B_L1_AMP_ADDR);
	sc_des1_inst.alarm_data->f40b_l2_amp_reg = Xil_In32(F40B_L2_AMP_ADDR);
	sc_des1_inst.alarm_data->f40b_l3_amp_reg = Xil_In32(F40B_L3_AMP_ADDR);
	sc_des1_inst.alarm_data->f40b_n_amp_reg = Xil_In32(F40B_N_AMP_ADDR);
	sc_des1_inst.alarm_data->f20a_l1_amp_reg = Xil_In32(F20A_L1_AMP_ADDR);
	sc_des1_inst.alarm_data->f20a_n_amp_reg = Xil_In32(F20A_N_AMP_ADDR);
	sc_des1_inst.alarm_data->f20b_l2_amp_reg = Xil_In32(F20B_L2_AMP_ADDR);
	sc_des1_inst.alarm_data->f20b_n_amp_reg = Xil_In32(F20B_N_AMP_ADDR);
	sc_des1_inst.alarm_data->fbyp_l1_amp_reg = Xil_In32(FBYP_L1_AMP_ADDR);
	sc_des1_inst.alarm_data->fbyp_l2_amp_reg = Xil_In32(FBYP_L2_AMP_ADDR);
	sc_des1_inst.alarm_data->fbyp_l3_amp_reg = Xil_In32(FBYP_L3_AMP_ADDR);
	sc_des1_inst.alarm_data->fbyp_n_amp_reg = Xil_In32(FBYP_N_AMP_ADDR);
}

// Alarm limits are not likely to be changed often, so we setup a structure with all of the limits and load them at once
// we can't use MemCpy as it causes an overflow of the AXI bus in the FPGA.
void set_alarm_limits(ALARM_LIMIT_T *alarm_limits_value){ //alarm limits populated in
	Xil_MemCpy((void *)MAIN_L1_VOLT_MAX_ADDR, &alarm_limits_value->main_l1_volt_max_reg, sizeof(ALARM_LIMIT_T));
//	Xil_Out32(MAIN_L1_VOLT_MAX_ADDR,alarm_limits_value->main_l1_volt_max_reg);
//	Xil_Out32(MAIN_L1_VOLT_MIN_ADDR,alarm_limits_value->main_l1_volt_min_reg);
//	Xil_Out32(MAIN_L2_VOLT_MAX_ADDR,alarm_limits_value->main_l2_volt_max_reg);
//	Xil_Out32(MAIN_L2_VOLT_MIN_ADDR,alarm_limits_value->main_l2_volt_min_reg);
//	Xil_Out32(MAIN_L3_VOLT_MAX_ADDR,alarm_limits_value->main_l3_volt_max_reg);
//	Xil_Out32(MAIN_L3_VOLT_MIN_ADDR,alarm_limits_value->main_l3_volt_min_reg);
//
//	Xil_Out32(MAIN_FREQ_MAX_ADDR,alarm_limits_value->main_freq_max_reg);
//	Xil_Out32(MAIN_FREQ_MIN_ADDR,alarm_limits_value->main_freq_min_reg);
//	Xil_Out32(MAIN_PHASE_MAX_ADDR,alarm_limits_value->main_phase_max_reg);
//	Xil_Out32(MAIN_PHASE_MIN_ADDR,alarm_limits_value->main_phase_min_reg);
//
//	Xil_Out32(MAIN_L1_AMP_MAX_ADDR,alarm_limits_value->main_l1_amp_max_reg);
//	Xil_Out32(MAIN_L2_AMP_MAX_ADDR,alarm_limits_value->main_l2_amp_max_reg);
//	Xil_Out32(MAIN_L3_AMP_MAX_ADDR,alarm_limits_value->main_l3_amp_max_reg);
//	Xil_Out32(MAIN_N_AMP_MAX_ADDR,alarm_limits_value->main_n_amp_max_reg);
//
//	Xil_Out32(F60A_L1_AMP_MAX_ADDR,alarm_limits_value->f60a_l1_amp_max_reg);
//	Xil_Out32(F60A_L2_AMP_MAX_ADDR,alarm_limits_value->f60a_l2_amp_max_reg);
//	Xil_Out32(F60A_L3_AMP_MAX_ADDR,alarm_limits_value->f60a_l3_amp_max_reg);
//	Xil_Out32(F60A_N_AMP_MAX_ADDR,alarm_limits_value->f60a_n_amp_max_reg);
//
//	Xil_Out32(F60B_L1_AMP_MAX_ADDR,alarm_limits_value->f60b_l1_amp_max_reg);
//	Xil_Out32(F60B_L2_AMP_MAX_ADDR,alarm_limits_value->f60b_l2_amp_max_reg);
//	Xil_Out32(F60B_L3_AMP_MAX_ADDR,alarm_limits_value->f60b_l3_amp_max_reg);
//	Xil_Out32(F60B_N_AMP_MAX_ADDR,alarm_limits_value->f60b_n_amp_max_reg);
//
//	Xil_Out32(F40A_L1_AMP_MAX_ADDR,alarm_limits_value->f40a_l1_amp_max_reg);
//	Xil_Out32(F40A_L2_AMP_MAX_ADDR,alarm_limits_value->f40a_l2_amp_max_reg);
//	Xil_Out32(F40A_L3_AMP_MAX_ADDR,alarm_limits_value->f40a_l3_amp_max_reg);
//	Xil_Out32(F40A_N_AMP_MAX_ADDR,alarm_limits_value->f40a_n_amp_max_reg);
//
//	Xil_Out32(F40B_L1_AMP_MAX_ADDR,alarm_limits_value->f40b_l1_amp_max_reg);
//	Xil_Out32(F40B_L2_AMP_MAX_ADDR,alarm_limits_value->f40b_l2_amp_max_reg);
//	Xil_Out32(F40B_L3_AMP_MAX_ADDR,alarm_limits_value->f40b_l3_amp_max_reg);
//	Xil_Out32(F40B_N_AMP_MAX_ADDR,alarm_limits_value->f40b_n_amp_max_reg);
//
//	Xil_Out32(F20A_L1_AMP_MAX_ADDR,alarm_limits_value->f20a_l1_amp_max_reg);
//	Xil_Out32(F20A_N_AMP_MAX_ADDR,alarm_limits_value->f20a_n_amp_max_reg);
//	Xil_Out32(F20B_L2_AMP_MAX_ADDR,alarm_limits_value->f20b_l2_amp_max_reg);
//	Xil_Out32(F20B_N_AMP_MAX_ADDR,alarm_limits_value->f20b_n_amp_max_reg);
//
//	Xil_Out32(FBYP_L1_AMP_MAX_ADDR,alarm_limits_value->fbyp_l1_amp_max_reg);
//	Xil_Out32(FBYP_L2_AMP_MAX_ADDR,alarm_limits_value->fbyp_l2_amp_max_reg);
//	Xil_Out32(FBYP_L3_AMP_MAX_ADDR,alarm_limits_value->fbyp_l3_amp_max_reg);
//	Xil_Out32(FBYP_N_AMP_MAX_ADDR,alarm_limits_value->fbyp_n_amp_max_reg);




	Xil_MemCpy(&sc_des1_inst.alarm_data->main_l1_volt_max_reg,alarm_limits_value,sizeof(ALARM_LIMIT_T));
//	return XST_SUCCESS;
}


// Set the timestamp based on the system time (normally used by default)
void set_timestamp(){
	struct timeval tv;
#ifdef PETALINUX
	gettimeofday(&tv, NULL);
#else
	tv.tv_sec = 0;
	tv.tv_usec = 0;
#endif
	u32 seconds_high = (u64)tv.tv_sec >> 32;
	u32 seconds_low = (u64)tv.tv_sec && 0x00000000ffffffff;
	u32 nanoseconds = tv.tv_usec*1000;
	set_timestamp_direct(seconds_high, seconds_low, nanoseconds);
}

// explicitly set the timestamp based on seconds/nanoseconds
void set_timestamp_direct(u32 seconds_high, u32 seconds_low, u32 nanoseconds) {
	Xil_Out32(TIMESTAMP_SECOND_MSB_ADDR, seconds_high);
	Xil_Out32(TIMESTAMP_SECOND_LSB_ADDR, seconds_low);
	Xil_Out32(TIMESTAMP_NANOSECOND_ADDR, nanoseconds);

	 get_timestamp();
}


// Interrupt status is obtained in the interrupt service routine only.  Move data to shadow registers
void get_interrupt_status(){
	sc_des1_inst.alarm_data->volt_int_event_reg.reg = voltage_interrupt_event.reg;
	sc_des1_inst.alarm_data->freq_int_event_reg.reg = frequency_interrupt_event.reg;
	sc_des1_inst.alarm_data->keypad_interrupt_reg.reg = keypad_interrupt.reg;
	sc_des1_inst.alarm_data->current_int_event_reg.reg = current_interrupt_event.reg;
	sc_des1_inst.alarm_data->stream_interrupt_reg.reg = stream_interrupt.reg;
	sc_des1_inst.alarm_data->volt_interrupt_reg.reg = VoltageStatus.reg;
	sc_des1_inst.alarm_data->freq_interrupt_reg.reg = FreqStatus.reg;
	sc_des1_inst.alarm_data->current_interrupt_reg.reg = CurrentStatus.reg;
}

void set_control_reg(CONTROL_REG_T *control_reg_value) {
	Xil_Out32(CONTROL_REG_ADDR, control_reg_value->reg);
	sc_des1_inst.alarm_data->control_reg.reg = control_reg_value->reg;
}

void get_control_reg() {
	CONTROL_REG_T control_reg_value;
	control_reg_value.reg = Xil_In32(CONTROL_REG_ADDR);
	sc_des1_inst.alarm_data->control_reg.reg = control_reg_value.reg;
}


void get_contactor_reg() {
	sc_des1_inst.alarm_data->contactor_disengage_reg.reg = Xil_In32(CONTACTOR_DISENGAGE_ADDR);
}

void get_circuitbreaker_reg(){
	sc_des1_inst.alarm_data->circuit_breaker_status_reg.reg = Xil_In32(CIRCUIT_BREAKER_STATUS_ADDR);
}

void get_timestamp(){
	sc_des1_inst.alarm_data->timestamp_seconds_msb = Xil_In32(TIMESTAMP_SECOND_MSB_ADDR);
	sc_des1_inst.alarm_data->timestamp_seconds_lsb = Xil_In32(TIMESTAMP_SECOND_LSB_ADDR);
	sc_des1_inst.alarm_data->timestamp_nanoseconds = Xil_In32(TIMESTAMP_NANOSECOND_ADDR);
}

void get_keypad_values(){
	sc_des1_inst.alarm_data->keypad_value_reg.reg = Xil_In32(KEYPAD_VALUE_ADDR);
}

void set_contactor(CONTACTOR_DISENGAGE_T contactor_setting){
	Xil_Out32(CONTACTOR_DISENGAGE_ADDR, contactor_setting.reg);
	sc_des1_inst.alarm_data->contactor_disengage_reg.reg = contactor_setting.reg;
}

void set_system_frequency(int freq) {
	sc_des1_inst.alarm_data->control_reg.bits.freq60_50 = freq;
	set_control_reg(&sc_des1_inst.alarm_data->control_reg);
}

