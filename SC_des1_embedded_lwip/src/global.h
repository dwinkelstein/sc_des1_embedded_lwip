/*
 * Copyright (c) 2021 Aura Technologies, LLC
 *
 */

#ifndef TAC_PWR_GLOBAL_H
#define TAC_PWR_GLOBAL_H
#include "testdata.h"
#include "SC_des1.h"


   #define CLIENT_DEVICE_ID    12345678900
   #define MAXEVENTS                   64
   #define MAX_CLIENTS                 10
   #define MAX_INITIALIZING_ENTRIES    10

   #define MAX_MESSAGE_SIZE             3
   #define DEVICE_ID_LOC                0
   #define MESSAGE_ID_LOC               8
   #define MESSAGE_LOC                 12
   #define DATA_LENGTH_LOC             16
   #define MESSAGE_DATA_LOC            20

   #define ALARM_REG_SIZE               4    // all alarm registers are same size
   #define ALARM_REG_COUNT              4    // number of alarm registers
   #define CONFIG_LIMITS_COUNT         38    // number of limit values stored in the configuration
   #define CONFIG_FACTORS_COUNT         9    // number of correction factors stored in the configuration
   #define CONFIG_PRIORITY_COUNT        6    // number of feed shed priorities stored in the configuration
   #define SINGLE_CYCLE_BLOCKS          8    // number of single cycles sets in snapshot data
   #define SNAPSHOT_DATA_COUNT         37    // number of data points in each block
   #define REALTIME_DATA_COUNT         36    // number of data points in each block

   #define CONFIG_DATA_SIZE           244    // KLM:todo - remove later
   #define SNAPSHOT_DATA_SIZE         316

   #define ISVALIDSOCKET(s) ((s) >= 0)
   #define SOCKET int

   typedef struct
   {
      long long      deviceID;
      unsigned int   messageID;
      char           cmd[4];           // must be 4 only using 3 - boundary issue with compiler
      int            dataLength;       // length of payload data block
      void           *messageData;     // allowing for different types of data
   }  MessageStruct;

   enum messageIdentifier {
         NO_MSG = 0,
         ACK_MSG,
         FEED_SHED_PRIORITY_MSG,
         HB_MSG,
         GET_ALARMS_MSG,
         GET_CONFIG_MSG,
         GET_REALTIME_DATA_MSG,
         GET_SNAPSHOT_DATA_MSG,
		 GET_TEMPERATURE_MSG,
		 RESPONSE_TEMPERATURE_MSG,
		 GET_VERSION_MSG,
		 VERSION_MSG,
         NO_DATA_MSG,
         NAK_MSG,
         ALARM_RESPONSE_MSG,
         CONFIG_UPDATE_MSG,
         SNAPSHOT_DATA_MSG,
         REALTIME_DATA_MSG,
         SET_ALARM_LIMITS_MSG,
         SET_CONFIG_MSG,
         SET_ENABLE_MSG,
         SET_INPUT_FREQUENCY_MSG,
         SET_LEDS_MSG,
         SET_MONITORING_MSG,
         SET_FEED_CONTROL_MSG,
         SET_STREAMING_MSG,
         SET_TIME_MSG,
         ILLEGAL_MSG,
         GET_NODATA_MSG,
         END_OF_MAP
   };

   enum alarmLocation {
      VOLTAGE_LOC = 0,        // bit locations for each alarm type
      CURRENT_LOC,            // data is stored in the alarmType field of the alarmData structure
      PHASE_LOC,
      FREQUENCY_LOC,
      CONTACTOR_LOC,
      CIRCUIT_BREAKER_LOC
   };


   enum serverState {
      UNKNOWN,
      DISCONNECTED,
      INITIALIZING,
      ACTIVE
   };

   typedef struct {
      long long               deviceID;                  // use device ID as key
      int                     fd;                        // file descriptor
      int                     messageID;                 // message index counter
      serverState             state;                     // current state of the connection
      enum messageIdentifier  expectedResponse;          // expected response message from last command
      int                     realtimeBlocksCount;       // number of realtime data blocks in last response
      unsigned long           controlReg;
      unsigned long           contactorState;
      unsigned char           enableOperation;
      unsigned char           inputFrequency;            // 0 - 60Hz, 1 - 50Hz
      unsigned char           enableLeds;
      unsigned char           enableMonitoring;
      unsigned char           enableStreaming;
      unsigned char           feedControl;
      void               *currentAlarmData;   // This is a cludge, this is really alarmData type
      alarmLimits             *currentAlarmLimits;
      feedShedPriority        *currentFeedShedPriorities;
      correctionFactors       *currentCorrectionFactors;
      snapshotData            *currentSnapshotData;
      realtimeData            *currentRealtimeData;      // temporary - not for final solution
   } serverConnection;

   struct sendMsgQueueElement {
	   void *outputBuffer;
	   serverConnection *currentServer;
	   int byteCount;
   } ;
   #define SEND_MSG_QUEUE_LEN 32 /*Update V24 to fix controller crash on multiple event*/


 extern serverConnection     currentServer;
 extern void initialization ( void );
 extern int messageParser( unsigned char *msg, serverConnection *currentServer );
 extern err_t nextSendMsg();
 extern struct tcp_pcb *pcb;
 extern int responseParser( enum messageIdentifier newOutputMsg,
                            char msgDataByte,
                            serverConnection *currentServer );
#endif // TAC_PWR_GLOBAL_H


