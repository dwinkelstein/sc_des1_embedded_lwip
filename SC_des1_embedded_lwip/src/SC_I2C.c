/*
 * Standalone_test.c
 *
 *  Created on: Jan 4, 2021
 *      Author: DanWinkelstein
 */

/******************************************************************************/
/*****************************************************************************/
/**
 * @file Standalone_test.c
 *
 * This file contains the DC2329A Standalone test software.
 * This software reads
 * 1. Enables capture of Voltage, Current and Power factor from the FPGA code for the
 *    DC2329A setup in the lab using the standalone_data_aquisition project (DC_alone)
 * 2. Reads the temperature from the ZCU104 temperature sensor
 * 3. Enables pushbutton and LED control using polled mode
 *
 *
 * <pre> MODIFICATION HISTORY:
 *
 * Ver	 Who Date    	Changes
 * ----- --- -------- 	-----------------------------------------------
 * 1.00a dw 01/04/2021 	First release
 *
 * </pre>
 *
 ****************************************************************************/

/***************************** Include Files **********************************/
#include "SC_des1.h"

/************************** Constant Definitions ******************************/

/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */
#define IIC_DEVICE_ID		XPAR_XIICPS_0_DEVICE_ID
#define MUX_ADDR            0x70  // TCA9543 device
#define MUX_PORT            0x01  // SCboard I2C is on port 0
#define IIC_SCLK_RATE       400000  //  400K SCL rate
#define LM75_ADDR           0x48  // LM75 device with A0,A1,A2 = GND
#define LM75_TEMP_REG       0x00  // temperature register pointer
#define PCA9539_ADDR        0x74  // PCA9539 device with A1,A0 = GND
#define PCA9539_IN_PORT0_ADDR 0x00 // PCA9539  r/o register
#define PCA9539_IN_PORT1_ADDR 0x01 // PCA9539  r/o register
#define PCA9539_OUT_PORT0_ADDR 0x02 // PCA9539  r/w register
#define PCA9539_OUT_PORT1_ADDR 0x03 // PCA9539  r/w register
#define PCA9539_POL_PORT0_ADDR 0x04 // PCA9539  r/w register
#define PCA9539_POL_PORT1_ADDR 0x05 // PCA9539  r/w register
#define PCA9539_CONF_PORT0_ADDR 0x06 // PCA9539  r/w register
#define PCA9539_CONF_PORT1_ADDR 0x07 // PCA9539  r/w register

/********************************externals **************************************/
extern SC_DES1_T sc_des1_inst;
/************************** Variable Definitions ******************************/

XIicPs Iic;			/* Instance of the IIC Device */
XIicPs_Config *Config_XIicPS;
int Status;

/************************** I2C device initialization ******************************/

int SC_I2C_Init(int DeviceID){
	/*
	 * Run the Iic Self Test example, specify the Device ID that is
	 * generated in xparameters.h

	 * Initialize the IIC driver so that it's ready to use
	 * Look up the configuration in the config table, then initialize it.
	 */
	Config_XIicPS = XIicPs_LookupConfig(XPAR_XIICPS_0_DEVICE_ID);
	if (NULL == Config_XIicPS) {
		return XST_FAILURE;
	}

	Status = XIicPs_CfgInitialize(&Iic, Config_XIicPS, Config_XIicPS->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Perform a self-test.
	 */
	Status = XIicPs_SelfTest(&Iic);
	if (Status != XST_SUCCESS) {
		xil_printf("IIC Self Test Failed\r\n");
		return XST_FAILURE;
	}

	Status = XIicPs_SetSClk(&Iic, IIC_SCLK_RATE);

	return XST_SUCCESS;
}


int SC_I2C_setMux(){
	/*
	 * Setup the MUX (TCA95483) so we are looking at the devices on the SC board
	 * MUX address is 0x70
	 * SC board devices are on port 0
	 */
	u8 write_data = MUX_PORT;
	u8 mux_read_data;

	Status = XIicPs_MasterSendPolled(&Iic, &write_data, 1, MUX_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("Write MUX Failed\r\n");
		return XST_FAILURE;
	}
	Status = XIicPs_MasterRecvPolled(&Iic, &mux_read_data, 1, MUX_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("Read MUX Failed\r\n");
		return XST_FAILURE;
	}
	usleep(1000);

	return XST_SUCCESS;

}

// get the temperature of the system by reading the LM75 temperature register via I2C

int get_temperature(){
	SC_I2C_setMux();  // set the mux to point to the SC board
	/**
	 * Read from the LM75 to get the temperature
	 * LM75 address is 0x48
	 * Temperature Register is at address 0x00
	 */

	u8 write_data = LM75_TEMP_REG;
	u8 lm75_read_data[2];

	Status = XIicPs_MasterSendPolled(&Iic, &write_data, (s32) 1, (u16) LM75_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("Write LM75 Failed\r\n");
		return XST_FAILURE;
	}
	Status = XIicPs_MasterRecvPolled(&Iic, lm75_read_data, (s32) 2, (u16) LM75_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("Read INA226 Failed\r\n");
		return XST_FAILURE;
	}
	// data comes back with degrees in lm75_read_data[0] as sign extended s8
	// bit 7 of lm75_read_data[1] adds 0.5
	// sign extend the data
	s32 temperature;
	if(lm75_read_data[0] & 0x80){
		temperature = lm75_read_data[0] | 0xffffff00;
	} else {
		temperature = lm75_read_data[0] & 0xff;
	}
	sc_des1_inst.temperature = (double)temperature;
	if(lm75_read_data[1]) // bit 7 add 0.5
		sc_des1_inst.temperature += 0.5;
	return XST_SUCCESS;
}

int config_pca9539(u8 config0, u8 config1){
	SC_I2C_setMux();  // set the mux to point to the SC board
	u8 write_data[3];
	write_data[0] = PCA9539_CONF_PORT0_ADDR;
	write_data[1] = config0;
	write_data[2] = config1;
	u8 pca9539_read_data[2];
	Status = XIicPs_MasterSendPolled(&Iic, (u8 *)&write_data, (s32) 3, (u16) PCA9539_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("Write PCA9539 Failed\r\n");
		return XST_FAILURE;
	}
//	usleep(1000);
	write_data[0] = PCA9539_CONF_PORT0_ADDR;
//	XIicPs_SetOptions(&Iic, XIICPS_REP_START_OPTION);  // read requires repeat start;
	Status = XIicPs_MasterSendPolled(&Iic, (u8 *)&write_data, (s32) 1, (u16) PCA9539_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("Write PCA9539 Failed\r\n");
		return XST_FAILURE;
	}
//	usleep(1000);
//	XIicPs_ClearOptions(&Iic, XIICPS_REP_START_OPTION);  // Clear repeat start;
	Status = XIicPs_MasterRecvPolled(&Iic, pca9539_read_data, (s32) 2, (u16) PCA9539_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("Read PCA9539 Failed\r\n");
		return XST_FAILURE;
	}
	printf("PCA control register %x %x\n", pca9539_read_data[0],pca9539_read_data[1]);
	return XST_SUCCESS;
}

// Get the circuit breaker and contactor status by reading the PCA9539 GPIO expander
int get_pca9539_status() {  // I2C function read CB trip and Contactor Open
	SC_I2C_setMux();  // set the mux to point to the SC board
	/**
	 * Read from the PCA9539 ports 0 and 1 to get the circuit breaker and contactor status
	 * LM75 address is 0x74
	 * port 0 is at address 0x00
	 * port 1 is at address 0x01
	 */
	u8 write_data = PCA9539_IN_PORT0_ADDR;
	u8 pca9539_read_data[2];

//	XIicPs_SetOptions(&Iic, XIICPS_REP_START_OPTION);  // read requires repeat start;
	Status = XIicPs_MasterSendPolled(&Iic, &write_data, (s32) 1, (u16) PCA9539_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("Write PCA9539 Failed\r\n");
		return XST_FAILURE;
	}
//	XIicPs_ClearOptions(&Iic, XIICPS_REP_START_OPTION);  // Clear repeat start;
	Status = XIicPs_MasterRecvPolled(&Iic, pca9539_read_data, (s32) 2, (u16) PCA9539_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("Read PCA9539 Failed\r\n");
		return XST_FAILURE;
	}
#ifdef DEBUG
	printf("PCA port register[1:0] %x %x \n", pca9539_read_data[1],pca9539_read_data[0]);
#endif
	sc_des1_inst.pca9539->port0.reg = pca9539_read_data[0];
	sc_des1_inst.pca9539->port1.reg = pca9539_read_data[1];

   return XST_SUCCESS;
}

// Use the PCA9539 to turn the LED off=0 on=1 (on is default)
int set_pca9539_LED(int off_on) { // I2C function to turn LED's OFF = 0 or ON = 1
	SC_I2C_setMux();  // set the mux to point to the SC board
	// write to the configuration register to set bit 7 or port 0 to read/write
	u8 write_data[2];
	write_data[0] = PCA9539_CONF_PORT0_ADDR;
	write_data[1] = 0x7F;
	u8 pca9539_read_data;

	Status = XIicPs_MasterSendPolled(&Iic, &write_data[0], (s32) 2, (u16) PCA9539_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("Write PCA9539 Failed\r\n");
		return XST_FAILURE;
	}
#ifdef DEBUG
	// read from to the configuration register0 and verify bit 7 has been clear
//	XIicPs_SetOptions(&Iic, XIICPS_REP_START_OPTION);  // read requires repeat start;
	write_data[0] = PCA9539_CONF_PORT0_ADDR;
	Status = XIicPs_MasterSendPolled(&Iic, &write_data[0], (s32) 1, (u16) PCA9539_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("Write PCA9539 Failed\r\n");
		return XST_FAILURE;
	}

//	XIicPs_ClearOptions(&Iic, XIICPS_REP_START_OPTION);  // read requires repeat start;
	Status = XIicPs_MasterRecvPolled(&Iic, &pca9539_read_data, (s32) 1, (u16) PCA9539_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("Read PCA9539 Failed\r\n");
		return XST_FAILURE;
	}
	printf("PCA config port register %x \n", pca9539_read_data);
#endif

	// write to the output register and set bit 7
	write_data[0] = PCA9539_OUT_PORT0_ADDR;
	write_data[1] = off_on << 7;
	Status = XIicPs_MasterSendPolled(&Iic, &write_data[0], (s32) 2, (u16) PCA9539_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("Write PCA9539 Failed\r\n");
		return XST_FAILURE;
	}
#ifdef DEBUG
	// read from to the output register and verify bit 7 has been set
//	XIicPs_SetOptions(&Iic, XIICPS_REP_START_OPTION);  // read requires repeat start;
	write_data[0] = PCA9539_OUT_PORT0_ADDR;
	Status = XIicPs_MasterSendPolled(&Iic, &write_data[0], (s32) 1, (u16) PCA9539_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("Write PCA9539 Failed\r\n");
		return XST_FAILURE;
	}

//	XIicPs_ClearOptions(&Iic, XIICPS_REP_START_OPTION);  // read requires repeat start;
	Status = XIicPs_MasterRecvPolled(&Iic, &pca9539_read_data, (s32) 1, (u16) PCA9539_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("Read PCA9539 Failed\r\n");
		return XST_FAILURE;
	}

	printf("PCA port register %x \n", pca9539_read_data);
	if( Status == XST_SUCCESS){
		sc_des1_inst.pca9539->port0.bits.led_off = pca9539_read_data >> 7;
		return XST_SUCCESS;
	} else
		return XST_FAILURE;
#else
	sc_des1_inst.pca9539->port0.bits.led_off = off_on;
	return XST_SUCCESS;
#endif


}


